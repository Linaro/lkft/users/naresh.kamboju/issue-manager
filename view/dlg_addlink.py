from PySide6 import QtWidgets

from failures_models import *
from issues_models import *


class AddLinkDialog(QtWidgets.QDialog):
    def __init__(self, general_data):
        self.general_data = general_data
        self.db_cursor = self.general_data["db"]

    def get_link_types(self):
        query = """
            SELECT *
              FROM linktypes
        """
        linktypes = []
        for row in self.general_data["db"]["cursor"].execute(query):
            linktypes.append(row)
        pass
        return linktypes

    def get_dialog(self):
        self.dialog = self.general_data["loader"].load("dlg-addlink.ui", None)

        linktypes = self.get_link_types()

        for linktype in linktypes:
            self.dialog.combo_linktype.addItem(linktype[1], userData=linktype[0])

        return self.dialog
