from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtUiTools import QUiLoader

import view.dlg_addlink
import view.dlg_confirmmerge
import view.dlg_viewlog
from failures_models import *
from fakesquad import FakeSquad
from issues_models import *


class FailuresWindow(QtWidgets.QMainWindow):
    class NumberSortModel(QtCore.QSortFilterProxyModel):
        def __init__(self, mlist=None):
            super().__init__()
            self._items = [] if mlist == None else mlist
            self._header = []

        # def data(self, index, role):
        #     if role == QtCore.Qt.DisplayRole:
        #         return self._items[index.row()][index.column()]

        # def rowCount(self, parent=None):
        #     return len(self._items)

        # def columnCount(self, parent=None):
        #     row_count =  self.rowCount(self)
        #     if row_count > 0:
        #         return len(self._items[0])
        #     else:
        #         return 0

        # def setData(self, index, value, role=QtCore.Qt.EditRole):
        #     if value is not None and role == QtCore.Qt.EditRole:
        #         self._items[index.row()][index.column()] = value
        #         return True
        #     return False

        def lessThan(self, left, right):
            # print("Sorting!")
            if left.data().isdigit():
                lvalue = int(left.data())
                rvalue = int(right.data())
            else:
                lvalue = left.data()
                rvalue = right.data()
            return lvalue < rvalue

    def __init__(self, general_data):
        self.general_data = general_data
        self.db_cursor = self.general_data["db"]["cursor"]
        super().__init__()

    # not working:
    def dragEnterEvent(self, e):
        print("dragEnterEvent")
        e.accept()

    # not working:
    def dropEvent(self, e):
        print("dropEvent")
        position = e.position()
        widget = e.source()
        row = self.tbl_failures.rowAt(
            position.y()
            - self.tbl_failures.horizontalHeader().height()
            - self.tbl_failures.y()
        )
        column = self.tbl_failures.columnAt(position.x() - self.tbl_failures.x())
        self.tbl_issues.setCellWidget(row, column, widget)
        e.accept()

    def create_new_issue(self, proposed_description=False):
        if not proposed_description:
            proposed_description = ""
        description, ok = QtWidgets.QInputDialog.getText(
            None,
            "New issue",
            "Description of new issue:",
            QtWidgets.QLineEdit.Normal,
            proposed_description,
        )

        issue_id = None
        if ok:
            issue_id = self.general_data["all_issues"].create_issue(description)

        return issue_id, description, ok

    def fill_knownfailures(self, failures_id_list):
        def selection_changed(index_selected, index_deselected):
            selected_failures = (
                self.window.tbl_knownfailures.selectionModel().selectedRows()
            )

            selected_count = len(selected_failures)

            if selected_count == 1:
                failure_id = selected_failures[0].data()
                failure = self.general_data["all_failures"].get(failure_id)
                self.update_data(failure)
            else:
                self.update_data(None)

        def click(index):
            # print("click @ %d,%d!" % (index.row(), index.column()))
            # failure_id = index.siblingAtColumn(0).data()
            # failure = self.general_data["active_failures"].get(failure_id)
            pass

        def doubleclick(index):
            # print("doubleclick @ %d,%d!" % (index.row(), index.column()))
            # failure_id = index.siblingAtColumn(0).data()
            pass

        if failures_id_list == None:
            return
        if type(failures_id_list) == list and len(failures_id_list) == 0:
            if "knownfailures" in self.general_data["datamodel"]:
                self.general_data["datamodel"]["knownfailures"].setRowCount(0)
            # self.window.tbl_knownfailures.viewport().update()
            return

        print("fill_knownfailures")
        window = self.window
        total_failures = len(failures_id_list)

        # model = QtGui.QStandardItemModel(total_failures, 6)
        self.general_data["datamodel"]["knownfailures"] = QtGui.QStandardItemModel(
            total_failures, 6
        )

        self.general_data["datamodel"]["knownfailures"].setHorizontalHeaderLabels(
            ["_id", "_all", "Test", "Env.", "Score", "Build ID"]
        )

        # filter proxy model
        filter_proxy_model = self.NumberSortModel()
        # filter_proxy_model = QtCore.QSortFilterProxyModel()
        filter_proxy_model.setSourceModel(
            self.general_data["datamodel"]["knownfailures"]
        )
        filter_proxy_model.setFilterKeyColumn(1)
        window.filter_new_failures.textChanged.connect(
            filter_proxy_model.setFilterRegularExpression
        )
        window.tbl_knownfailures.setModel(filter_proxy_model)
        self.general_data["fx"]["config_set"]("init_tbl_knownfailures", True)

        current_row = 0
        for failure_id in failures_id_list:
            failure = self.general_data["all_failures"].get(failure_id)

            if not failure:
                continue

            id_str = f"{failure.id}"
            name_str = f"{failure.testsuite}/{failure.short_name}"
            env_str = self.general_data["squad"]["fake"].resolve_environment(
                self.general_data, failure.squad_environment_id
            )
            score_str = "%d" % failure.confidence_score
            build_str = self.general_data["squad"]["fake"].resolve_build(
                self.general_data, failure.squad_build_id
            )
            log_str = f"{failure.log}"
            all_data_str = f"{name_str} {env_str} {score_str} {build_str} {log_str}"

            id = QtGui.QStandardItem(id_str)
            name = QtGui.QStandardItem(name_str)
            env = QtGui.QStandardItem(env_str)
            score = QtGui.QStandardItem(score_str)
            build = QtGui.QStandardItem(build_str)
            all_data = QtGui.QStandardItem(all_data_str)

            self.general_data["datamodel"]["knownfailures"].setItem(current_row, 0, id)
            self.general_data["datamodel"]["knownfailures"].setItem(
                current_row, 1, all_data
            )
            self.general_data["datamodel"]["knownfailures"].setItem(
                current_row, 2, name
            )
            self.general_data["datamodel"]["knownfailures"].setItem(current_row, 3, env)
            self.general_data["datamodel"]["knownfailures"].setItem(
                current_row, 4, score
            )
            self.general_data["datamodel"]["knownfailures"].setItem(
                current_row, 5, build
            )

            current_row += 1

        self.general_data["datamodel"]["knownfailures"].setRowCount(current_row)
        # window.tbl_knownfailures.clicked.connect(click)
        # window.tbl_knownfailures.doubleClicked.connect(doubleclick)

        window.tbl_knownfailures.show()
        window.tbl_knownfailures.hideColumn(0)
        window.tbl_knownfailures.hideColumn(1)
        window.tbl_knownfailures.setColumnWidth(2, 320)
        window.tbl_knownfailures.setColumnWidth(4, 60)
        window.tbl_knownfailures.setSelectionBehavior(
            QtWidgets.QAbstractItemView.SelectionBehavior.SelectRows
        )
        window.tbl_knownfailures.selectionModel().selectionChanged.connect(
            selection_changed
        )

    def update_data(self, object):
        if object == None:
            self.window.txt_data.setPlainText("")
            return

        if type(object) == str:
            print("object = str")
            failure_id = int(object)
            if not failure_id:
                self.window.txt_data.setPlainText("")
                return

            failure = self.general_data["active_failures"].get(failure_id)
            name_str = f"{failure.testsuite}/{failure.short_name}"
            env_str = self.general_data["squad"]["fake"].resolve_environment(
                self.general_data, failure.squad_environment_id
            )
            html_data = f"<html><head><title>{name_str}</title></head>"
            html_data += f"<body><h1>{name_str} ({env_str})</h1><h2>Log</h2><hr/>"
            if failure.log:
                html_data += f"<pre>{failure.log}</pre>"
            html_data += "<hr/></body></html>"
            self.window.txt_data.setHtml(html_data)

        if type(object) == Failure:
            # print("object = failure")
            failure_id = object.id

            if not failure_id:
                self.window.txt_data.setPlainText("")
                return

            failure = object
            name_str = f"{failure.testsuite}/{failure.short_name}"
            env_str = self.general_data["squad"]["fake"].resolve_environment(
                self.general_data, failure.squad_environment_id
            )
            html_data = f"<html><head><title>{name_str}</title></head>"
            html_data += f"<body><h1>{name_str} ({env_str})</h1><h2>Log</h2><hr/>"
            if failure.log:
                html_data += f"<pre>{failure.log}</pre>"
            html_data += "<hr/></body></html>"
            self.window.txt_data.setHtml(html_data)

        if type(object) == Issue:
            # print("object = issue")
            issue_id = object.id
            if not issue_id:
                self.window.txt_data.setPlainText("")
                return

            issue = self.general_data["all_issues"].get(issue_id)
            extra_info = self.general_data["all_issues"].extra_info(issue_id)
            description_str = f"{issue.description}"
            html_data = f"<html><head><title>{description_str}</title></head>"
            html_data += f"<body><h1>{description_str}</h1>"
            html_data += "<p>"
            html_data += "Failures in this issue: <b>{failures_count}</b><br/>".format(
                **extra_info
            )

            if extra_info["datetime_first"]:
                first = datetime.datetime.fromtimestamp(
                    int(extra_info["datetime_first"]), tz=datetime.timezone.utc
                )
                html_data += "First time seen: " + first.strftime("%Y-%m-%d") + "<br/>"
            if extra_info["datetime_last"]:
                last = datetime.datetime.fromtimestamp(
                    int(extra_info["datetime_last"]), tz=datetime.timezone.utc
                )
                html_data += "Last time seen: " + last.strftime("%Y-%m-%d") + "<br/>"

            html_data += (
                "Seen in <b>" + str(len(extra_info["build_versions"])) + "</b> builds: "
            )
            html_data += ", ".join(sorted(extra_info["build_versions"])) + "<br/>"

            set_environments = set(extra_info["environments"])
            sorted_unique_list = sorted(list(set(set_environments)))
            html_data += (
                "Seen in <b>" + str(len(sorted_unique_list)) + "</b> environments: "
            )
            html_data += ", ".join(sorted_unique_list) + "<br/>"

            if len(extra_info["links"]) > 0:
                html_data += "<p>"
                for link_type, link_url in extra_info["links"]:
                    html_data += f'<a href="{link_url}">{link_type}</a><br/>'
                html_data += "</p>"

            for note in extra_info["notes"]:
                note_date = datetime.datetime.fromtimestamp(
                    int(note), tz=datetime.timezone.utc
                )
                html_data += (
                    "<p>Note from "
                    + note_date.strftime("%Y-%m-%d %H:%M UTC")
                    + ":<br/>"
                )
                html_data += extra_info["notes"][note]
                html_data += "<br/></p>"

            html_data += "<hr/></body></html>"
            self.window.txt_data.setHtml(html_data)

    def fill_failures(self):
        def selection_changed(index_selected, index_deselected):
            selected_failures = self.window.tbl_failures.selectionModel().selectedRows()

            selected_count = len(selected_failures)

            if selected_count == 1:
                failure_id = selected_failures[0].data()
                failure = self.general_data["active_failures"].get(failure_id)
                self.update_data(failure)
            else:
                self.update_data(None)

        def click(index):
            # print("click @ %d,%d!" % (index.row(), index.column()))
            # failure_id = index.siblingAtColumn(0).data()
            # failure = self.general_data["active_failures"].get(failure_id)
            pass

        def doubleclick(index):
            # print("doubleclick @ %d,%d!" % (index.row(), index.column()))
            # failure_id = index.siblingAtColumn(0).data()
            pass

        print("fill_failures")
        self.general_data["active_failures"].fetch_active()
        window = self.window
        total_failures = len(self.general_data["active_failures"])

        # model = QtGui.QStandardItemModel(total_failures, 6)
        self.general_data["datamodel"]["failures"] = QtGui.QStandardItemModel(
            total_failures, 6
        )

        # window.tbl_failures.setHorizontalHeaderLabels(["Test", "Env", "Score", "Build ID"])
        self.general_data["datamodel"]["failures"].setHorizontalHeaderLabels(
            ["_id", "_all", "Test", "Env.", "Score", "Build ID"]
        )

        # filter proxy model
        filter_proxy_model = self.NumberSortModel()
        filter_proxy_model.setSourceModel(self.general_data["datamodel"]["failures"])
        filter_proxy_model.setFilterKeyColumn(1)
        window.filter_new_failures.textChanged.connect(
            filter_proxy_model.setFilterRegularExpression
        )
        window.tbl_failures.setModel(filter_proxy_model)
        self.general_data["state"]["init_tbl_failures"] = True

        print("Total active failures: " + str(total_failures))
        self.general_data["datamodel"]["failures"].setRowCount(0)
        for row_count in range(0, total_failures):
            failure = self.general_data["active_failures"].get_at(row_count)

            id_str = f"{failure.id}"
            name_str = f"{failure.testsuite}/{failure.short_name}"
            env_str = self.general_data["squad"]["fake"].resolve_environment(
                self.general_data, failure.squad_environment_id
            )
            score_str = "%d" % failure.confidence_score
            build_str = self.general_data["squad"]["fake"].resolve_build(
                self.general_data, failure.squad_build_id
            )
            log_str = f"{failure.log}"
            all_data_str = f"{name_str} {env_str} {score_str} {build_str} {log_str}"

            id = QtGui.QStandardItem(id_str)
            name = QtGui.QStandardItem(name_str)
            env = QtGui.QStandardItem(env_str)
            score = QtGui.QStandardItem(score_str)
            build = QtGui.QStandardItem(build_str)
            all_data = QtGui.QStandardItem(all_data_str)

            self.general_data["datamodel"]["failures"].setItem(row_count, 0, id)
            self.general_data["datamodel"]["failures"].setItem(row_count, 1, all_data)
            self.general_data["datamodel"]["failures"].setItem(row_count, 2, name)
            self.general_data["datamodel"]["failures"].setItem(row_count, 3, env)
            self.general_data["datamodel"]["failures"].setItem(row_count, 4, score)
            self.general_data["datamodel"]["failures"].setItem(row_count, 5, build)

        # window.tbl_failures.clicked.connect(click)
        # window.tbl_failures.doubleClicked.connect(doubleclick)

        window.tbl_failures.show()
        window.tbl_failures.hideColumn(0)
        window.tbl_failures.hideColumn(1)
        window.tbl_failures.setColumnWidth(2, 320)
        window.tbl_failures.setColumnWidth(4, 60)
        window.tbl_failures.setSelectionBehavior(
            QtWidgets.QAbstractItemView.SelectionBehavior.SelectRows
        )
        window.tbl_failures.selectionModel().selectionChanged.connect(selection_changed)

    def fill_issues(self):
        def selection_changed(index_selected, index_deselected):
            # count() doesn't really count:
            if index_selected.count() == 1:
                issue_id = index_selected.indexes()[0].data()
                issue = self.general_data["all_issues"].get(issue_id)
                self.update_data(issue)
                # self.general_data["all_issues"].extra_info(issue_id)

                failures_id_list = self.general_data[
                    "all_issues"
                ].get_failures_for_issue(issue_id)
                self.fill_knownfailures(failures_id_list)
            else:
                self.update_data(None)
                self.fill_knownfailures([])

        def click(index):
            # print("click @ %d,%d!" % (index.row(), index.column()))
            # issue_id = index.siblingAtColumn(0).data()
            pass

        def doubleclick(index):
            # print("doubleclick @ %d,%d!" % (index.row(), index.column()))
            issue_id = index.siblingAtColumn(0).data()
            issue = self.general_data["all_issues"].get(issue_id)

            # dialog rename
            new_description, ok = QtWidgets.QInputDialog.getText(
                None,
                "Rename issue",
                "New issue description:",
                QtWidgets.QLineEdit.Normal,
                issue.description,
            )
            if ok:
                print("description: " + new_description)
                self.general_data["all_issues"].rename_issue(issue_id, new_description)
                # update item here
                index.model().dataChanged.emit(index, index)
                # description = QtGui.QStandardItem(new_description)
                # all_data = QtGui.QStandardItem(new_description)
                # model = index.model()
                # model.setItem(index.row(), 1, all_data)
                # model.setItem(index.row(), 2, description)

        print("fill_issues")
        self.general_data["all_issues"].read_from_database()

        window = self.window
        total_issues = len(self.general_data["all_issues"])
        model = QtGui.QStandardItemModel(total_issues, 3)

        model.setHorizontalHeaderLabels(["_id", "_all", "Issue"])

        # filter proxy model
        filter_proxy_model = QtCore.QSortFilterProxyModel()
        filter_proxy_model.setSourceModel(model)
        filter_proxy_model.setFilterKeyColumn(1)
        window.filter_issues.textChanged.connect(
            filter_proxy_model.setFilterRegularExpression
        )

        for row_count in range(0, total_issues):
            issue = self.general_data["all_issues"].get_at(row_count)

            id_str = f"{issue.id}"
            description_str = f"{issue.description}"

            id = QtGui.QStandardItem(id_str)
            description = QtGui.QStandardItem(description_str)
            all_data = QtGui.QStandardItem(description_str)

            model.setItem(row_count, 0, id)
            model.setItem(row_count, 1, all_data)
            model.setItem(row_count, 2, description)

        window.tbl_issues.clicked.connect(click)
        window.tbl_issues.doubleClicked.connect(doubleclick)

        window.tbl_issues.setModel(filter_proxy_model)
        window.tbl_issues.show()
        window.tbl_issues.hideColumn(0)
        window.tbl_issues.hideColumn(1)
        window.tbl_issues.setColumnWidth(2, 400)
        window.tbl_issues.setSelectionBehavior(
            QtWidgets.QAbstractItemView.SelectionBehavior.SelectRows
        )
        window.tbl_issues.selectionModel().selectionChanged.connect(selection_changed)

    def debug_datachanged_slot(self, first, last):
        # Helper to show dataChanged signal is emitted
        print(f"dataChanged() emitted with ({first}, {last})")

    def get_window(self):
        # not working:
        def dropEvent(self, e):
            print("dropEvent")
            position = e.position()
            widget = e.source()
            row = self.tbl_failures.rowAt(
                position.y()
                - self.tbl_failures.horizontalHeader().height()
                - self.tbl_failures.y()
            )
            column = self.tbl_failures.columnAt(position.x() - self.tbl_failures.x())
            self.tbl_issues.setCellWidget(row, column, widget)
            e.accept()

        def on_failures_context_menu(point):
            failure_id = None
            issue_id = None

            index = self.window.tbl_failures.indexAt(point)
            # print(index)
            failure_id = index.siblingAtColumn(0).data()

            issues_indexes = self.window.tbl_issues.selectionModel().selectedRows()
            if len(issues_indexes) > 0:
                issue_index = issues_indexes[0]
                issue_id = issue_index.siblingAtColumn(0).data()
                # issue_id = self.window.tbl_issues.in

            # create context menu
            self.context_menu = QtWidgets.QMenu(self.window)
            action_merge = QtGui.QAction(
                "Merge failure (L) with issue (C)", self.window
            )
            action_create_new = QtGui.QAction("Create new issue", self.window)
            action_viewlog = QtGui.QAction("View complete log", self.window)
            action_rerun = QtGui.QAction("Re-run", self.window)
            action_bisect = QtGui.QAction("Bisect", self.window)

            self.context_menu.addAction(action_merge)
            self.context_menu.addAction(action_create_new)
            self.context_menu.addAction(action_viewlog)
            self.context_menu.addSeparator()
            self.context_menu.addAction(action_rerun)
            self.context_menu.addAction(action_bisect)

            action = self.context_menu.exec_(
                self.window.tbl_failures.mapToGlobal(point)
            )

            if action == action_merge:
                print("Action: merge!")
                confirmmerge_dialog = view.dlg_confirmmerge.ConfirmMergeDialog(
                    self.general_data
                )

                if not issue_id:
                    return

                failure_id_list = []
                failure_indexes = (
                    self.window.tbl_failures.selectionModel().selectedRows()
                )
                for f in failure_indexes:
                    failure_id_list.append(f.siblingAtColumn(0).data())
                print(failure_id_list)

                self.dialog = confirmmerge_dialog.get_dialog(failure_id_list, issue_id)
                self.dialog.exec_()
                if self.dialog.result():
                    # "OK"
                    self.general_data["active_failures"].merge_with_issue(
                        failure_id_list, issue_id
                    )
                    # self.general_data["active_failures"].read_from_database()
                    self.fill_failures()
                    self.window.tbl_failures.viewport().update()
                else:
                    # "Cancel"
                    pass
            elif action == action_create_new:
                print("Action: create new!")
                # Get failure test name
                failure_indexes = (
                    self.window.tbl_failures.selectionModel().selectedRows()
                )

                failure_id_list = []
                for f in failure_indexes:
                    failure_id_list.append(f.siblingAtColumn(0).data())
                print(failure_id_list)

                proposed_description = ""
                if len(failure_id_list) > 0:
                    failure_id = failure_id_list[0]
                    selected_failure = self.general_data["active_failures"].get(
                        failure_id
                    )
                    proposed_description = (
                        f"{selected_failure.testsuite}/{selected_failure.short_name}"
                    )

                issue_id, description, ok = self.create_new_issue(proposed_description)

                self.general_data["all_issues"].read_from_database()
                self.window.tbl_failures.viewport().update()

                if ok:
                    print("description: " + description)
                    self.general_data["active_failures"].merge_with_issue(
                        failure_id_list, issue_id
                    )
                    self.fill_failures()
                    self.fill_issues()

            elif action == action_viewlog:
                viewlog_dialog = view.dlg_viewlog.ViewLogDialog(self.general_data)
                self.dialog = viewlog_dialog.get_dialog(failure_id)
                self.dialog.show()
                pass
            elif action == action_rerun:
                print("Action: rerun!")
                self.fill_issues()
                self.fill_failures()
            elif action == action_bisect:
                print("Action: bisect!")
            else:
                print("Action: other!")

        def on_issues_context_menu(point):
            # failure_id = None
            issue_id = None

            index = self.window.tbl_issues.indexAt(point)
            # print(index)
            issue_id = index.siblingAtColumn(0).data()

            failures_indexes = self.window.tbl_failures.selectionModel().selectedRows()
            failure_id_list = []
            for f in failures_indexes:
                failure_id_list.append(f.siblingAtColumn(0).data())

            # if len(failure_id_list) > 0:
            #     # failure_index = failures_indexes[0]
            #     # failure_id = failure_index.siblingAtColumn(0).data()
            #     failure_id = failure_id_list[0]

            # create context menu
            self.context_menu = QtWidgets.QMenu(self.window)
            action_merge = QtGui.QAction(
                "Merge failure (L) with issue (C)", self.window
            )
            action_note = QtGui.QAction("Add note", self.window)
            action_link = QtGui.QAction("Add link", self.window)
            action_rerun = QtGui.QAction("Re-run", self.window)
            action_bisect = QtGui.QAction("Bisect", self.window)
            action_resolve = QtGui.QAction("Mark as resolved", self.window)

            self.context_menu.addAction(action_merge)
            self.context_menu.addAction(action_note)
            self.context_menu.addAction(action_link)
            self.context_menu.addSeparator()
            self.context_menu.addAction(action_rerun)
            self.context_menu.addAction(action_bisect)
            self.context_menu.addSeparator()
            self.context_menu.addAction(action_resolve)

            action = self.context_menu.exec_(self.window.tbl_issues.mapToGlobal(point))

            if action == action_merge:
                print("Action: merge!")
                confirmmerge_dialog = view.dlg_confirmmerge.ConfirmMergeDialog(
                    self.general_data
                )

                if not issue_id:
                    return

                self.dialog = confirmmerge_dialog.get_dialog(failure_id_list, issue_id)
                self.dialog.exec_()
                if self.dialog.result():
                    # "OK"
                    print(failure_id_list)
                    self.general_data["active_failures"].merge_with_issue(
                        failure_id_list, issue_id
                    )
                    # self.general_data["active_failures"].read_from_database()
                    self.fill_failures()
                else:
                    # "Cancel"
                    pass
            elif action == action_resolve:
                print("Action: resolved!")

                # dialog resolve
                issue = self.general_data["all_issues"].get(issue_id)
                description_str = f"{issue.description}"
                resolve_dialog = QtWidgets.QMessageBox()
                resolve_dialog.setWindowTitle("Resolve issue")
                resolve_dialog.setText(
                    "Do you want to mark issue '" + description_str + "' as resolved?"
                )
                resolve_dialog.setStandardButtons(
                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Cancel
                )
                resolve_dialog.setDefaultButton(QtWidgets.QMessageBox.Yes)
                button = resolve_dialog.exec_()

                if button == QtWidgets.QMessageBox.Yes:
                    print("Yes!")
                    self.general_data["all_issues"].resolve_issue(issue_id)
                    self.fill_issues()

            elif action == action_note:
                note, ok = QtWidgets.QInputDialog.getText(
                    None,
                    "New note",
                    "Enter note:",
                    QtWidgets.QLineEdit.Normal,
                    "",
                )

                pass
                if ok:
                    self.general_data["all_issues"].create_note(issue_id, note)
                    issue = self.general_data["all_issues"].get(issue_id)
                    self.update_data(issue)

            elif action == action_link:
                addlink_dialog = view.dlg_addlink.AddLinkDialog(self.general_data)
                self.dialog = addlink_dialog.get_dialog()
                self.dialog.exec_()
                if self.dialog.result():
                    # "OK"
                    link = self.dialog.ln_url.text()
                    linktype = self.dialog.combo_linktype.currentData()
                    self.general_data["all_issues"].create_link(issue_id, linktype, link)
                    issue = self.general_data["all_issues"].get(issue_id)
                    self.update_data(issue)
                    pass
                else:
                    # "Cancel"
                    pass

            elif action == action_rerun:
                print("Action: rerun!")
                self.fill_issues()
                self.fill_failures()
            elif action == action_bisect:
                print("Action: bisect!")
            else:
                print("Action: other!")

        def on_knownfailures_context_menu(point):
            failure_id = None
            issue_id = None

            index = self.window.tbl_knownfailures.indexAt(point)
            # print(index)
            failure_id = index.siblingAtColumn(0).data()

            issues_indexes = self.window.tbl_issues.selectionModel().selectedRows()
            if len(issues_indexes) > 0:
                issue_index = issues_indexes[0]
                issue_id = issue_index.siblingAtColumn(0).data()
                # issue_id = self.window.tbl_issues.in

            # create context menu
            self.context_menu = QtWidgets.QMenu(self.window)
            action_unmerge = QtGui.QAction(
                "Remove failure (R) from issue (C)", self.window
            )
            action_viewlog = QtGui.QAction("View complete log", self.window)
            action_rerun = QtGui.QAction("Re-run", self.window)
            action_bisect = QtGui.QAction("Bisect", self.window)

            self.context_menu.addAction(action_unmerge)
            self.context_menu.addAction(action_viewlog)
            self.context_menu.addSeparator()
            self.context_menu.addAction(action_rerun)
            self.context_menu.addAction(action_bisect)

            action = self.context_menu.exec_(
                self.window.tbl_knownfailures.mapToGlobal(point)
            )

            if action == action_unmerge:
                print("Action: unmerge!")
                # confirmunmerge_dialog = view.dlg_confirmunmerge.ConfirmUnmergeDialog(
                #     self.general_data
                # )

                if not issue_id:
                    return

                # self.dialog = confirmunmerge_dialog.get_dialog(failure_id, issue_id)
                # self.dialog.exec_()
                # if self.dialog.result():
                # "OK"
                failure_indexes = (
                    self.window.tbl_knownfailures.selectionModel().selectedRows()
                )
                failure_id_list = []
                for f in failure_indexes:
                    failure_id_list.append(f.siblingAtColumn(0).data())
                print(failure_id_list)
                self.general_data["active_failures"].unmerge_from_issue(
                    failure_id_list, issue_id
                )
                self.fill_failures()
                self.window.tbl_failures.viewport().update()
                # self.general_data["active_failures"].read_from_database()
                # self.fill_failures()
                # else:
                #     # "Cancel"
                #     pass
            elif action == action_viewlog:
                viewlog_dialog = view.dlg_viewlog.ViewLogDialog(self.general_data)
                self.dialog = viewlog_dialog.get_dialog(failure_id)
                self.dialog.show()
                pass
            elif action == action_rerun:
                print("Action: rerun!")
                self.fill_issues()
                self.fill_failures()
            elif action == action_bisect:
                print("Action: bisect!")
            else:
                print("Action: other!")

        loader = QUiLoader()
        self.window = loader.load("view-failures.ui", None)

        self.window.setWindowTitle(f"Failures")

        self.window.tbl_failures.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.window.tbl_failures.customContextMenuRequested.connect(
            on_failures_context_menu
        )

        self.window.tbl_issues.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.window.tbl_issues.customContextMenuRequested.connect(
            on_issues_context_menu
        )

        self.window.tbl_knownfailures.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.window.tbl_knownfailures.customContextMenuRequested.connect(
            on_knownfailures_context_menu
        )

        # No luck:
        # self.window.tbl_failures.setDragEnabled(True)
        # self.window.tbl_failures.setAcceptDrops(True)
        # self.window.tbl_issues.setAcceptDrops(True)
        # self.window.tbl_issues.setDragEnabled(True)

        self.window.action_new_issue.triggered.connect(self.create_new_issue)

        self.fill_failures()
        self.fill_issues()
        self.window.setAcceptDrops(True)
        return self.window
