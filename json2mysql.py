#!/usr/bin/env python3
import json
import os
import sys

import MySQLdb
from dotenv import load_dotenv


def get_squad_api_id(url):
    elements = url.split("/")
    return elements[-2]


if len(sys.argv) < 2:
    print("Need a file.json as argument")
    sys.exit(0)

filename = sys.argv[1]
file = open(filename)
json_data = json.load(file)

load_dotenv()
con = MySQLdb.connect(
    host=os.getenv("HOST"),
    user=os.getenv("USERNAME"),
    passwd=os.getenv("PASSWORD"),
    db=os.getenv("DATABASE"),
    autocommit=True,
)
cur = con.cursor()

for page in json_data:
    for failure in page["results"]:
        testsuite = failure["name"].split("/")[0]
        try:
            query_form = "INSERT INTO failures VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
            data = (
                int(failure["id"]),
                testsuite,
                failure["short_name"],
                failure["log"],
                failure["confidence"]["score"],
                int(failure["confidence"]["count"]),
                int(get_squad_api_id(failure["build"])),
                int(get_squad_api_id(failure["environment"])),
                int(get_squad_api_id(failure["test_run"])),
                int(get_squad_api_id(failure["suite"])),
                int(get_squad_api_id(failure["metadata"])),
            )
            res = cur.execute(query_form, data)
            print(".", end="")
        except Exception as e:
            print(e)
            print("!", end="")
            pass
con.commit()
