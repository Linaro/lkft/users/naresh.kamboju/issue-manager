#!/usr/bin/env python3
import json
import os
import sqlite3
import sys

from PySide6 import QtWidgets
from PySide6.QtUiTools import QUiLoader

import failures_models
import issues_models
import view.failures
from fakesquad import FakeSquad

general_data = {}
general_data["config"] = {}
general_data["datamodel"] = {}
general_data["db"] = {}
general_data["fx"] = {}
general_data["squad"] = {}
general_data["state"] = {}


def config_get(config, item):
    """
    Get config item(s) from configuration dict.

    Specific item: "item/from/config".
    Set of items: "item/from" (will return all items therein).
    """
    parts = item.split("/")

    if len(parts) == 1:
        if parts[0] in config:
            return config[parts[0]]
    elif len(parts) > 1:
        if parts[0] in config:
            if parts[1] in config[parts[0]]:
                return config[parts[0]][parts[1]]
    return None


def config_set(config_string, value):
    general_data["config"][config_string] = value


general_data["fx"]["config_get"] = config_get
general_data["fx"]["config_set"] = config_set

try:
    config_file = open("config.json")
    general_data["config"] = json.load(config_file)
except FileNotFoundError as e:
    print("Can't find %s: %s" % ("config.json", str(e)))
    sys.exit(1)

try:
    os.makedirs(config_get(general_data["config"], "squad/cache_dir"))
except FileNotFoundError as e:
    print(
        "Could not create directory "
        + config_get(general_data["config"], "squad/cache_dir")
    )
except FileExistsError as e:
    pass

app = QtWidgets.QApplication(sys.argv)

if len(sys.argv) > 1:
    print(len(sys.argv))

loader = QUiLoader()
general_data["loader"] = loader

con = sqlite3.connect("lim.db")
cur = con.cursor()

general_data["db"]["cursor"] = cur
general_data["db"]["conn"] = con
general_data["all_failures"] = failures_models.Failures(general_data)
general_data["active_failures"] = failures_models.Failures(general_data)
general_data["all_issues"] = issues_models.Issues(general_data)
general_data["squad"]["fake"] = FakeSquad(general_data)

failures_window = view.failures.FailuresWindow(general_data)
general_data["all_failures"].fetch_all()
# general_data["active_failures"].fetch_active()
# general_data["all_issues"].read_from_database()

window = failures_window.get_window()

window.show()

app.exec()
