#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

set -e
BUILD_ID="$1"

if [ ! "${BUILD_ID}" = "${BUILD_ID//./}" ]; then
  # kernelversion, e.g. "6.4.1-rc1"
  git_describe="$(get_build_id_for_kernelversion "${BUILD_ID}")"
  branch="$(get_branch_from_makefile_version "${BUILD_ID}")"
  BUILD_ID="$(get_squad_build_id_for_git_describe "${branch}" "${git_describe}")"
fi

results_json="$(squad_download_and_cache "${SQUAD_SERVER}/api/builds/${BUILD_ID}/failures_with_confidence" 200 "" 1)"
echo "results.json: ${results_json}"
jq -s '.' "${results_json}" > "${BUILD_ID}.results.json"
echo "Results: ${BUILD_ID}.results.json"

# Get build date and project
build_json="$(download_and_cache "${SQUAD_SERVER}/api/builds/${BUILD_ID}/")"

created_at="$(jq -r .created_at "${build_json}")"
ts_created_at="$(date +%s -d "${created_at}")"

squad_project_url="$(jq -r .project "${build_json}")"
squad_project="${squad_project_url/${SQUAD_SERVER}/}"
squad_project="${squad_project/\/api\/projects\//}"
squad_project="${squad_project/\//}"

echo "INSERT INTO builds VALUES(${BUILD_ID}, ${squad_project}, ${ts_created_at}, \"FETCHED\");"
